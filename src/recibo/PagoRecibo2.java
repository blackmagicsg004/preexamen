/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recibo;

/**
 *
 * @author Black
 */
public class PagoRecibo2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
            // TODO code application logic here
        PagoRecibo PagoRecibo2 = new PagoRecibo();
        PagoRecibo2.setNumCon(102);
        PagoRecibo2.setNomEmp("jose lopez");
        PagoRecibo2.setDom("av del sol 1200");
        PagoRecibo2.setTipoCon(1);
        PagoRecibo2.setNivelEst(1);
        PagoRecibo2.setPdBase(700.0f);
        PagoRecibo2.setDiasTrabajados(15);
        PagoRecibo2.setFechaPago("21 marzo 2019");
        
        System.out.println("Calculo de pago subtotal:"+ PagoRecibo2.calcularSubtotal());
        System.out.println("calculo de pago con impuesto:"+ PagoRecibo2.calcularImpuesto());
        System.out.println("calculo de pago total:"+ PagoRecibo2.calcularTotal());
        
        
        System.out.println("-----------------------------------------------------------------------");
        
        PagoRecibo2.setNumCon(103);
        PagoRecibo2.setNomEmp("maria acosta");
        PagoRecibo2.setDom("av del sol 1200");
        PagoRecibo2.setTipoCon(2);
        PagoRecibo2.setNivelEst(2);
        PagoRecibo2.setPdBase(700.0f);
        PagoRecibo2.setDiasTrabajados(15);
        PagoRecibo2.setFechaCon("20 marzo 2019");
        
        System.out.println("Calculo de pago subtotal:"+ PagoRecibo2.calcularSubtotal());
        System.out.println("calculo de pago con impuesto:"+ PagoRecibo2.calcularImpuesto());
        System.out.println("calculo de pago total:"+ PagoRecibo2.calcularTotal());
    }
}

